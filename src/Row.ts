import { styled } from "./stitches.config";

const Row = styled("div", {
  display: "flex",
  flexFlow: "row wrap",
});

export default Row;
