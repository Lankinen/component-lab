import Col from "./Col";
import Row from "./Row";
import Box from "./Box";
import Img from "./Img";
import Text from "./Text";
import Span from "./Span";

export { Col, Row, Box, Img, Text, Span };
