import { styled } from "./stitches.config";

const Col = styled("div", {
  position: "relative",
  maxWidth: "100%",
  minHeight: 1,
});

export default Col;
