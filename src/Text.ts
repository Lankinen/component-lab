import { styled } from "./stitches.config";

const Text = styled("p", { margin: 0 });

export default Text;
